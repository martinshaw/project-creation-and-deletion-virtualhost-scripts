#!/bin/sh

# !!! MUST BE RUN AS ROOT !!!

sudo service apache2 stop

sudo a2dissite $1.conf

sudo rm -rfv /var/www/$1/*
sudo rm -rfv /var/www/$1
sudo rm -rfv /etc/apache2/sites-available/$1.conf

sudo service apache2 restart

echo "Deleted Project: $1"
echo " You will now need to remove $1.lap1.local from the Windows etc host file"
