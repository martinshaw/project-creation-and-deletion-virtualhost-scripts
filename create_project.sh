#!/bin/sh

# !!! MUST BE RUN AS ROOT !!!

sudo service apache2 stop

sudo mkdir /var/www/$1

echo "<?php echo \"Hello, this is $1\"; ?>" > /var/www/$1/index.php

echo "<VirtualHost *:80>
	ServerName $1.lap1.local

	ServerAdmin developer@martinshaw.co
	DocumentRoot /var/www/$1

	ErrorLog \${APACHE_LOG_DIR}/error.log
	CustomLog \${APACHE_LOG_DIR}/access.log combined
</VirtualHost>
" > /etc/apache2/sites-available/$1.conf

sudo a2ensite $1.conf

sudo service apache2 restart

echo "Created New Project"
echo " - http://$1.lap1.local"
echo " - /var/www/$1"
echo "  "
echo "   You will now need to add the following line to the Windows etc host file"
echo "  "
echo "   127.0.0.1       $1.lap1.local"
